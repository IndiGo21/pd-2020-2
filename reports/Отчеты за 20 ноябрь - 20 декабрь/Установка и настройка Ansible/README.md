﻿# Отчет по установке и настраиванию Ansible 20 ноябрь - 20 декабрь 

#### Установка репозитория epel-realese для установки ansible . 

![](1.install_epel-realese.png)

#### Установка ansible. 

![](2.install_ansible.png)

#### Завершение установки. 

![](3.ansible_installation_complete.png)

#### Присвоение клиенту статического IP . 

![](4.static_IP_for_client.png)

#### Создал ssh ключ и отправил клиенту для подключения к нему. 

![](5.ssh_key_for_auth.png)

#### Создал в файле hosts клиента ansible. 

![](6.ansible_host.png)

#### Изменил файл конфигурации, добавив в него файл с хостами

![](7.ansble_cfg_for_hosts.png)

#### Проверка работоспособности ansible 

![](8.work_check.png)
