#!/usr/bin/env bash
set -o errexit
function cleanup {
rm -rf "$(pwd)"
}
cd "$(mktemp -d)" && trap cleanup EXIT
sudo dnf install epel-release -y
sudo dnf install ansible -y
ansible-galaxy collection install ansible.posix
ansible-galaxy collection install community.general
wget https://gitlab.com/IndiGo21/pd-2020-2/-/archive/master/pd-2020-2-master.tar?path=src --content-disposition 
sudo tar -xaf pd-2020-2-master-src.tar --strip-components=2 --exclude pd-2020-2-master-src/src/script.sh --directory /etc/ansible/ pd-2020-2-master-src/src/ 
