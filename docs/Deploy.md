# Инструкция по развертыванию Backend мобильного приложения банка Ак Барс

### Для работы с ansible требуется дистрибутив LInux <u>Centos 8</u> (на клиенте и на ansible мастере)

#### Запуск скрипта, который устанавливает ansible и все нужные для нее компоненты, также скачивает все нужные ansible скрипты, необходимые для развертывания backend из репозитория.

```bash
sudo dnf upgrade
sudo dnf install wget
wget https://gitlab.com/IndiGo21/pd-2020-2/-/raw/master/src/script.sh?inline=false -qO- | bash
```

#### Для запуска развертывания backend через ansible на локальном компьютере (на том же компьютере, где установлен ansible), выполните команду:

```bash
ansible-playbook /etc/ansible/deploy_backend.yml
```